/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author SEGUNDODAM
 */
public class procesador {
    private  int id ;
    private  String Marca ;
    private  String Socket;

    public procesador(int id, String Marca, String Socket) {
        this.id = id;
        this.Marca = Marca;
        this.Socket = Socket;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public String getSocket() {
        return Socket;
    }

    public void setSocket(String Socket) {
        this.Socket = Socket;
    }
    
}
