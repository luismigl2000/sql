/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package montaje;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Crud {
    
   
  public void ejecutar(){
      System.out.println("Que quire hacer :");  
     Scanner lector = new Scanner(System.in);
     String s = lector.nextLine();
   
        try{ 
      
        
           Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/montaje?characterEncoding=utf8";
            
            Connection con = DriverManager.getConnection(url, "root","d@m1920");
            switch(s){
                case "insert":
                   
                    System.out.println("Introduzca la tabla: ");
                    String t = lector.nextLine();
                    if(t.equals("caja")){
                        System.out.println("Itroduce un id nuevo(recuerde no repetir id):");
                        String a = lector.nextLine();
                        System.out.println("Introduce su formato entre  ATX, Micro ATX o Mini-ITX:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su marca:");
                        String c = lector.nextLine();
                        PreparedStatement statement = con.prepareStatement("INSERT INTO caja (id, Formato, Marca) VALUES (?, ?,?)"); {
                          statement.setString(1, a);
                          statement.setString(2, b);
                          statement.setString(3, c);
                          int rows = statement.executeUpdate();
                         if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                         System.out.println("-------------------------------------------");
                    }
                    }else if(t.equals("placas")){
                        System.out.println("Itroduce un id nuevo(recuerde no repetir id):");
                        String a = lector.nextLine();
                        System.out.println("Introduce su Socket:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su formato:");
                        String c = lector.nextLine();
                        PreparedStatement statement = con.prepareStatement("INSERT INTO placas (id, Socket, Formato) VALUES (?, ?,?)"); {
                          statement.setString(1, a);
                          statement.setString(2, b);
                          statement.setString(3, c);
                          int rows = statement.executeUpdate();
                         if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                         System.out.println("-------------------------------------------");
                    }
                    }else if(t.equals("procesador")){
                        System.out.println("Itroduce un id nuevo(recuerde no repetir id):");
                        String a = lector.nextLine();
                        System.out.println("Introduce su marca:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su Socket:");
                        String c = lector.nextLine();
                        PreparedStatement statement = con.prepareStatement("INSERT INTO procesador (id, Marca , Socket) VALUES (?, ?,?)"); {
                          statement.setString(1, a);
                          statement.setString(2, b);
                          statement.setString(3, c);
                          int rows = statement.executeUpdate();
                          if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                          System.out.println("-------------------------------------------");
                         }
                    }else{
                        System.out.println("No existe esa tabla solo existen caja , placas y procesador");
                        System.out.println("-------------------------------------------");
                    }
                    /* https://www.ict.social/java/jdbc/databases-in-java-jdbc-insert-update-delete-and-count*/
                   con.close();
                   this.ejecutar();
                    break;
                     case "select":
                    Statement st = con.createStatement();
                    System.out.println("Introduzca la tabla: ");
                    String f = lector.nextLine();
                    if(f.equals("caja")){
                      String query = "select * from caja ORDER BY id";
                     ResultSet rs = st.executeQuery(query);
                        System.out.println("-------------------------------------------");
                     while(rs.next()){
                    System.out.println("Id:" +  rs.getString(1) + ". Formato: " + rs.getString(2) + ". Marca: " + rs.getString(3));  
                    }
                     con.close();
                 }else if(f.equals("placas")){
                    String query = "select * from placas ORDER BY id";
                     ResultSet rs = st.executeQuery(query);
                      System.out.println("-------------------------------------------");
                     while(rs.next()){
                    System.out.println("Id:" +  rs.getString(1) + ". Socket: " + rs.getString(2) + ". Formato: " + rs.getString(3));  
                    }
                     System.out.println("-------------------------------------------");
                     con.close();         
                 }else if(f.equals("procesador")){
                   String query = "select * from procesador ORDER BY id";
                     ResultSet rs = st.executeQuery(query);
                      System.out.println("-------------------------------------------");
                     while(rs.next()){
                    System.out.println("Id:" +  rs.getString(1) + ". Marca: " + rs.getString(2) + ". Socket: " + rs.getString(3));  
                    }
                     System.out.println("-------------------------------------------");
                     con.close();          
                            }else{
                 System.out.println("No existe esa tabla solo existen caja , placas y procesador");   
                 System.out.println("-------------------------------------------");
                 } 
                 
                 this.ejecutar();
                    break;
               case "update":
                   System.out.println("Introduzca la tabla: ");
                    String m = lector.nextLine();
                    if(m.equals("caja")){
                        System.out.println("Itroduce su id:");
                        String a = lector.nextLine();
                        System.out.println("Introduce su nuevo formato entre  ATX, Micro ATX o Mini-ITX:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su nueva marca:");
                        String c = lector.nextLine();
                     PreparedStatement statement = con.prepareStatement("UPDATE caja SET Formato=? , Marca=? WHERE id=?"); {
                     statement.setString(1, b);
                     statement.setString(2, c);
                    statement.setString(3, a);
                    int rows = statement.executeUpdate();
                    if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                    System.out.println("-------------------------------------------");
                    }
                    }else if(m.equals("placas")){
                     System.out.println("Itroduce un id:");
                        String a = lector.nextLine();
                        System.out.println("Introduce un nuevo Socket:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su nuevo formato entre  ATX, Micro ATX o Mini-ITX:");
                        String c = lector.nextLine();
                        PreparedStatement statement = con.prepareStatement("UPDATE placas SET Socket=? , Formato=? WHERE id=?"); {
                          statement.setString(1, b);
                          statement.setString(2, c);
                          statement.setString(3, a);
                          int rows = statement.executeUpdate();
                         if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                         System.out.println("-------------------------------------------");
                    }   
                    }else if(m.equals("procesador")){
                      System.out.println("Itroduce el id:");
                        String a = lector.nextLine();
                        System.out.println("Introduce su nueva  Marca:");
                        String b = lector.nextLine();
                        System.out.println("Introduce su nuevo Socket:");
                        String c = lector.nextLine();  
                        PreparedStatement statement = con.prepareStatement("UPDATE procesador SET  Marca=? , Socket=?  WHERE id=?"); {
                          statement.setString(1, b);
                          statement.setString(2, c);
                          statement.setString(3, a);
                          int rows = statement.executeUpdate();
                          if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                          System.out.println("-------------------------------------------");
                         } 
                    }else{
                     System.out.println("No existe esa tabla solo existen caja , placas y procesador");   
                     System.out.println("-------------------------------------------");
                    }
                    this.ejecutar();
                    break;
                case "delete":
                    System.out.println("Introduzca la tabla: ");
                    String y = lector.nextLine();
                    if(y.equals("caja")){
                        System.out.println("Itroduce el id :");
                        String a = lector.nextLine();
                         PreparedStatement statement = con.prepareStatement("DELETE FROM caja WHERE id=?"); {
                        statement.setString(1, a);
                        int rows = statement.executeUpdate();
                        if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                        System.out.println("-------------------------------------------");
                        }
                    }else if(y.equals("placas")){
                       System.out.println("Itroduce el id :");
                        String a = lector.nextLine(); 
                        PreparedStatement statement = con.prepareStatement("DELETE FROM placas WHERE id=?"); {
                        statement.setString(1, a);
                        int rows = statement.executeUpdate();
                        if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                        System.out.println("-------------------------------------------");
                       }
                    }else if(y.equals("procesador")){
                        System.out.println("Itroduce el id :");
                        String a = lector.nextLine();
                        PreparedStatement statement = con.prepareStatement("DELETE FROM procesador WHERE id=?"); {
                        statement.setString(1, a);
                        int rows = statement.executeUpdate();
                        if(rows==1){System.out.println("El proceso fue exitoso");}
                        else{System.out.println("Hubo un error en el proceso");}
                        System.out.println("-------------------------------------------");
                    }
                    }else{
                     System.out.println("No existe esa tabla solo existen caja , placas y procesador");   
                     System.out.println("-------------------------------------------");
                    }
                    this.ejecutar();
                    break;
                case "close":
                    System.out.println("Cerrando.....");
                    break;
                 default:
                     System.out.println("Los comandos son insert select update y delete .Siempre con la primera letra en minusculas ");
                     System.out.println("-------------------------------------------");
                     this.ejecutar();
            
       }
       
       }catch (Exception e){
            e.printStackTrace();
        }
  } 
   
}
